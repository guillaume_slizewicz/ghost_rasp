var startTime = Date.now();
var lastRandomButton = -1;
var sinceLightTime = 0;
var time = 4000;
var decreaseTime = 100;
var score = 0;
var BUTTON_NUM = 0;
//var TIME = 0;

module.exports={
  // CORE GAME FUNCTIONS MAYBE TO REDO
  SimonSays:function(){
    console.log("initialising SimonSays");
  },
  BirdChasingGame:function(boards,btn_num,color){
    console.log("playing bird chasing game");
    BUTTON_NUM = btn_num;
    this.lightUpOneRandomButton(boards,color);
    console.log("button num:",BUTTON_NUM);
  },
  /*-----------------------*/
  //BASICS BLOCKS FOR INPUTS
  buttonInput: function(index){
    //this function should get buttons Input
    console.log("new button input:",index);
    return index;
  },
  timeFromStart: function() {
    var millis = Date.now() - startTime;
    return Math.floor(millis);
  },
  timeSinceLightUp: function(){
    var millis = Date.now()-sinceLightTime;
    return Math.floor(millis);
  },
  decreaseTiming: function(){
    time = time-decreaseTime;
    score =score+1;
    console.log(time);
    console.log(score);
    return [time,score];
  },
  resetTiming: function(){
    time = 4000;
  },
  /*----------------------*/
  //BASICs BLOCKs FOR EVENT
  getRandomButton:function(){
    var randomButton = Math.floor(Math.random()*4);
    console.log('new random button:',randomButton);
    lastRandomButton = randomButton;
    return randomButton;
  },
  getLastRandomButton:function(){
    console.log("last random button:",lastRandomButton);
    return lastRandomButton;
  },
  addToScore:function(score){
    console.log("adding:",inc,"to the score");
    score+=inc;
    return score;
  },
  addOneToScore:function(){
    console.log("adding 1 to the score");
    score= score + 1;
    return score;
  },
  ResetScore:function(){
    console.log("adding 1 to the score");
    score= 0;
    return score;
  },
  getRandomColor:function(){
    var r = Math.floor(Math.random()*255);
    var g = Math.floor(Math.random()*255);
    var b = Math.floor(Math.random()*255);
    var randomColor = [r,g,b];
    console.log('new random color:',randomColor);
    return randomColor;
  },

  /*----lighting button-----*/
  setLedsColor:function(boards,i,color){
    sinceLightTime = Date.now();
    var array = [];
    array[0]=this.getLSB(color[0]);
    array[1]=this.getMSB(color[0]);
    array[2]=this.getLSB(color[1]);
    array[3]=this.getMSB(color[1]);
    array[4]=this.getLSB(color[2]);
    array[5]=this.getMSB(color[2]);
    boards[i].sysexCommand([0x44,array[0],array[1],array[2],array[3],array[4],array[5]]);
  },
  getLSB:function(v){
    return (v & 0x7f);
  },
  getMSB:function(v){
    return (v >> 7);
  },
  initColor:function(boards,color) {
    for (var i = 0; i < BUTTON_NUM; i++) {
      this.setLedsColor(boards,i, color);
    }
  },
  ligthEachButtonWithAColor:function(boards,color,colors){
    for(var i=0;i<BUTTON_NUM;i++){
      var randomIndex = Math.floor(Math.random()*colors.length);
      var theColor = colors[randomIndex];
      colors.splice(randomIndex,1);
      this.setLedsColor(boards,i,color);
    }
  },

  lightUpOneRandomButton:function(boards,color){
    var numRandom = this.getRandomButton();
    for(var i=0;i<BUTTON_NUM;i++){
      if(i!=numRandom)this.setLedsColor(boards,i,[0,0,0]);
      else this.setLedsColor(boards,i,color);
    }
  },
  lightUpOneRandomButtonWithRandomColor:function(boards){
    this.lightUpOneRandomButton(boards,this.getRandomColor());
  },
  showLooseAnimation:function(boards){
    this.initColor(boards,[255,0,0]);
  }

};
