var http = require('http').createServer(handler); //require http server, and create server with function handler()
var fs = require('fs'); //require filesystem module
var io = require('socket.io')(http) //require socket.io module and pass the http object (server)
var score = 0;

http.listen(8080); //listen to port 8080


function handler (req, res) { //create server
  fs.readFile(__dirname + '/public/index.html', function(err, data) { //read file index.html in public folder
    if (err) {
      res.writeHead(404, {'Content-Type': 'text/html'}); //display 404 on error
      return res.end("404 Not Found");
    }
    res.writeHead(200, {'Content-Type': 'text/html'}); //write HTML
    res.write(data); //write data from index.html
    return res.end();
  });
}


io.sockets.on('connection', function (socket) {// WebSocket Connection    
  scorevalue=score;
  socket.emit('score', scorevalue); 
  console.log(scorevalue);
    //score = score+(1/2)


  /*JSON RULES ENGINE BASICS*/
  /*------- LOGIC COMPARATOR ---------*/

  //function to convert string operator into boolean

  var comparators = {
    "eq": function(a, b) {
      return a === b;
    },
    "lt": function(a, b) {
      return a < b;
    },
    "gt": function(a, b) {
      return a > b;
    },
    "lti": function(a, b) {
      return a <= b;
    },
    "gti": function(a, b) {
      return a >= b;
    }
  };

  /* Start of the game*/

  var fs = require("fs"); /* get fs module */
  console.log("\n *STARTING* \n");
  /// Load Rules of the Game
  var content = fs.readFileSync("ruleSetOne.json");
  // Define to JSON type
  var jsonContent = JSON.parse(content);
  /*---ALL FUNCTIONS RESTS IN A SEPARATE FILE----*/
  var functionsLibrary = require('/home/pi/Documents/buttonGame/functionsLib.js');
  /*--- GET BASICS INFOS FROM JSON FILE ---*/


  var pause = false;

  var gameType = jsonContent.gameType;
  var settings = jsonContent.settings;
  var logic = jsonContent.logic;

  console.log("GAME TYPE:", gameType);
  console.log("GAME SETTINGS:", settings);
  console.log("----------------------------");

  /* ALL THIS SHOULD DEPENDS ON "USE" IN JSON */
  /* AND PUT IN AN INTANCE OF INPUT BLOCK BUTTON */
  /*--------------------------------------------*/
  var BUTTON_NUM = 4; /* number of buttons*/

  var five = require("johnny-five"); /* get jhonny-five module/library*/

  /* define ports for arduinos*/
  var ports = [{
      id: "A",
      port: "/dev/ttyUSB0",
      repl: false
    },
    {
      id: "B",
      port: "/dev/ttyUSB1",
      repl: false
    },
    {
      id: "C",
      port: "/dev/ttyUSB2",
      repl: false
    },
    {
      id: "D",
      port: "/dev/ttyUSB3",
      repl: false
    }
  ];
  //Debounce debugging variable (I guess)

  var lastDebounceTime = [];
  var debounceDelay = 200;

  //config of arduino boards (I guess), where the button+debounce is
  var boards = new five.Boards(ports);
  var buttons = [];
  boards.on("ready", function() {
    for (var i = 0; i < BUTTON_NUM; i++) {
      lastDebounceTime[i] = Date.now();
      buttons[i] = new five.Button({
        pin: 3, /* pin for the button */
        isPullup: true,
        board: boards[i]
      });
    }
    if (jsonContent.init) {
      //console.log("exec init function:", jsonContent.init);
      functionsLibrary[jsonContent.init](boards, BUTTON_NUM, settings.baseColor);
      functionsLibrary.resetTiming();
    }
    buttons.forEach(function(val, i, a) {
      buttons[i].on('up', function() {
        if (!pause) {
          var now = Date.now();
          if (now - lastDebounceTime[i] > debounceDelay) {
            runLogic(i);
            lastDebounceTime[i] = Date.now();
          }
        }
      });
    });
  });

  //THIS IS WHERE WE EVALUATE IF THE RULES ARE TRUE OR FALSE
  function runLogic(index) {
    //For each blocks of rules in the json
    logic.conditions.forEach(function(rulesBlock) {
      var evaluate;
      //for each rule in an AND block
      if ("AND" in rulesBlock) {
        var tempBooleanArrayAnd = [];
        rulesBlock.AND.forEach(function(rule) {
          //console.log(rule);
          //is it a function or a fixed value
          var against = (rule.value.function) ? functionsLibrary[rule.value.function]() : rule.value;
          var toTest = (rule.if.function) ? functionsLibrary[rule.if.function](index) : rule.if;
          var compResult = comparators[rule.operator](toTest, against);
          tempBooleanArrayAnd.push(compResult);
          //console.log(compResult);
        });
        console.log("temp AND results array:", tempBooleanArrayAnd);
        evaluate = tempBooleanArrayAnd.every(function(item) {
          return item;
        });
        //console.log("AND result:", evaluate);
      }

      //for each rule in an OR block
      if ("OR" in rulesBlock) {
        var tempBooleanArrayOr = [];
        rulesBlock.OR.forEach(function(rule) {
          //console.log(rule);
          //is it a function or a fixed value
          var against = (rule.value.function) ? functionsLibrary[rule.value.function]() : rule.value;
          var toTest = (rule.if.function) ? functionsLibrary[rule.if.function]() : rule.if;
          var compResult = comparators[rule.operator](toTest, against);
          tempBooleanArrayOr.push(compResult);
          //console.log(compResult);
        });
        console.log("temp OR results array:", tempBooleanArrayOr);
        evaluate = tempBooleanArray.some(function(item) {
          return item;
        });
        //console.log("OR result:", evaluate);
      }
      console.log("GLOBAL RESULT OF THE LOGIC", evaluate);

      // THEN What should we trigger if the rules evaluate as true or false
      if (evaluate) {
        rulesBlock.OnTrue.forEach(function(e) {
          console.log('true event', e);
          functionsLibrary[e.event](boards, settings.baseColor, settings.colorPalette);
          console.log("adding 1 to the score");
          score= score + 1;
          console.log("score is " + score);
          scorevalue=score;
          socket.emit('score', scorevalue); 
        });
      } else {
        rulesBlock.OnFalse.forEach(function(e) {
          //console.log('false event', e);
          pause = true;
          functionsLibrary[e.event](boards, settings.baseColor);
          //QUICK PATCH TODO
          setTimeout(function() {
            console.log("RESET");
            functionsLibrary[jsonContent.init](boards, BUTTON_NUM, settings.baseColor);
            functionsLibrary.resetTiming();
            score= 0;
            console.log("score is " + score);
            scorevalue=score;
            socket.emit('score', scorevalue); 
            pause = false;
          }, 4000,); /* I guess this is the downtime after a game*/
        });
      }
    });
  }
});
